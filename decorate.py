"""Generate chemically similar atomic structures.

Usage "ppython decorate.y 0.1" where 0.1 is the substitution threshold. The
threshold can be omitted in which case the default threshold is used. Requires a
database named "c2db-structures.db" and outputs to a database named
"decorated.db".

Requires having ASR installed (from git) and using master branch, old-master
would not work.

A C2DB database can currently be found in
"/home/niflheim/mogje/projects/c2db-decoration-project/workdir/c2db-structures.db"
but who knows for how long.

The output database has various helpful key value pairs. To identify the original C2DB
material it contains (MOE=Material of Origin)

- from_id: ID of MOE "c2db-structures.db".
- from_folder: Folder of MOE
- from_formula: Formula of MOE
- from_uid: UID of MOE

Additionally key value pairs named sub_X_from, sub_X_to, sub_X_prob where X goes
from 0 to number of substitutions. The substitutions are always ordered from
least probably to most probably substitution which is helpful when selecting
materials that require a specific threshold e.g.:

    $ ase db decorated.db "sub_0_prob>0.25"

Lastly some miscelleanous non self-explanatory key value pairs:

- nsubs: Number of substitutions

To understand script start by reading "pipeline" function and go on from there.
"""
import sys
from itertools import product
from pathlib import Path
from typing import Any, Callable, Dict, Iterator, List, MutableSet, Tuple

import numpy as np
from ase import Atoms
from ase.data import atomic_numbers, chemical_symbols, covalent_radii
from ase.db import connect
from ase.db.core import AtomsRow, Database
from ase.utils import seterr
from asr.structureinfo import main as structureinfo
from asr.database.material_fingerprint import main as material_fingerprint
from asr.utils import timed_print

Substitution = Dict[int, int]
SubstitutionData = np.ndarray
Element = str
SubstitutionProb = Tuple[Element, Element, float]
SubstitutionProbabilityArray = np.ndarray
KeyValuePairs = Dict[str, Any]
PipelineStream = Iterator[Tuple[Atoms, KeyValuePairs]]


DEFAULT_THRESHOLD = 0.1


ELEMENTS_WHERE_WE_HAVE_SETUPS = [
    "H",
    "He",
    "Li",
    "Be",
    "B",
    "C",
    "N",
    "O",
    "F",
    "Ne",
    "Na",
    "Mg",
    "Al",
    "Si",
    "P",
    "S",
    "Cl",
    "Ar",
    "K",
    "Ca",
    "Sc",
    "Ti",
    "V",
    "Cr",
    "Mn",
    "Fe",
    "Co",
    "Ni",
    "Cu",
    "Zn",
    "Ga",
    "Ge",
    "As",
    "Se",
    "Br",
    "Kr",
    "Rb",
    "Sr",
    "Y",
    "Zr",
    "Nb",
    "Mo",
    "Ru",
    "Rh",
    "Pd",
    "Ag",
    "Cd",
    "In",
    "Sn",
    "Sb",
    "Te",
    "I",
    "Xe",
    # 6
    "Cs",
    "Ba",
    "La",
    "Hf",
    "Ta",
    "W",
    "Re",
    "Os",
    "Ir",
    "Pt",
    "Au",
    "Hg",
    "Tl",
    "Pb",
    "Bi",
    "Rn",
]


def pipeline(
    c2db_db: str = "c2db-structures.db",
    decorated_db: str = "decorated.db",
    threshold: float = 0.1,
) -> None:
    """Run decoration pipeline output to decorated_db.

    This function

    Parameters
    ----------
    c2db_db : str, optional
        Database with input atomic structures, by default "c2db-structures.db"
    decorated_db : str, optional
        Output database, by default "decorated.db"
    threshold : float, optional
        Substitution probability threshold, by default 0.1. Substitution is skipped
        if less likely than threshold.
    """
    c2db = connect(c2db_db)

    print("Building descriptors...")
    descriptors = build_descriptors(c2db)

    print("Generating structures...")
    stream = yield_database_rows_atoms(c2db)
    stream = log_stage("Filter ehull < 0.3")(filter)(
        lambda item: item[0].get("ehull", 1) < 0.3, stream
    )

    stream = decorate(stream, threshold)
    stream = compute_key_value_pairs(stream)
    stream = filter_duplicates(stream, descriptors)
    stream = remove_magnetic_moments(stream)

    with connect(decorated_db, append=False) as decorated:
        write_output_database(stream, decorated)


def log_stage(name: str) -> Callable:
    """Make decorator that can print number of items that comes through a generator.

    Parameters
    ----------
    name : str
        Stage name

    Returns
    -------
    Callable
        Decorator
    """

    def _decorator(func: Callable) -> Callable:
        def _wrapped(*args, **kwargs) -> Iterator[Any]:
            count = 0
            stream = func(*args, **kwargs)
            for item in stream:
                count += 1
                yield item
            print(f"Stage '{name}': {count} items.")

        return _wrapped

    return _decorator


def build_reduced_formula_descriptors(db: Database) -> MutableSet[str]:
    """Make reduced formula descriptors.

    Parameters
    ----------
    db : Database
        Input database

    Returns
    -------
    MutableSet[str]
        Descriptors.
    """
    descriptors = set()

    for row in db.select():
        red_form = make_reduced_formula(row.toatoms())
        descriptors.add(red_form)

    return descriptors


def apply_substitution(atoms: Atoms, substitution: Substitution) -> Atoms:
    """Apply a subsitution to an atomic structure.

    A substitution can be in the format dict(1=2, 4=5) which would map atomic
    numbers 1->2 and 4->5.

    Parameters
    ----------
    atoms : Atoms
        Input atomic structure
    substitution : Substitution
        Substitution mapping.

    Returns
    -------
    Atoms
        Substituted atomic structure
    """
    new_atoms = atoms.copy()
    new_numbers = [
        substitution[number] if number in substitution else number
        for number in atoms.numbers
    ]

    # Scale in-plane lattice vectors by the harmonic mean of the covalent_radii
    sf = np.product([covalent_radii[n] for n in new_numbers]) / np.product(
        [covalent_radii[n] for n in atoms.numbers]
    )
    sf = pow(sf, 1.0 / len(new_numbers))
    sf = [sf if atoms.pbc[n] else 1 for n in range(3)]
    newcell = np.diag(sf).dot(new_atoms.get_cell())
    new_atoms.set_cell(newcell, scale_atoms=True)

    # update the atomic numbers
    new_atoms.numbers = new_numbers
    return new_atoms


def find_substitutions(
    number: int, p_ab: SubstitutionProbabilityArray, threshold: float
) -> List[int]:
    """Find substitutions for element of number.

    Note that the subsitution also includes the input element itself.

    Parameters
    ----------
    number : int
        Atomic number
    p_ab : SubstitutionProbabilityArray
        Substitution probability
    threshold : float
        Exclude substitution when probability less than threshold

    Returns
    -------
    List[int]
        Proposed alternative atomic numbers.
    """
    row = p_ab[number]
    substitutions = np.where(row > threshold)[0]
    allowed_numbers = [atomic_numbers[e] for e in ELEMENTS_WHERE_WE_HAVE_SETUPS]
    possible_substitutions = [s for s in substitutions if s in allowed_numbers]
    return possible_substitutions


def generate_structures(
    prototype: Atoms, p_ab: SubstitutionProbabilityArray, threshold: float = 0.08
) -> Iterator[Substitution]:
    """Generate substitution mappings.

    Generates atomic number mappings like dict(1=2, 3=4) which represents
    substitutions between structures.

    Parameters
    ----------
    prototype : Atoms
        Input atomic structure to decorate.
    p_ab : SubstitutionProbabilityArray
        Substitution probabilities as array
    threshold : float, optional
        Exclude substitution when probability less than threshold, by default 0.08

    Yields
    -------
    Iterator[Substitution]
        Subsitution mapping.
    """
    unique_numbers = list(set(prototype.numbers))
    substitutions = [
        find_substitutions(number, p_ab, threshold) for number in unique_numbers
    ]
    for substitution in product(*substitutions):
        new_numbers = list(substitution)
        if new_numbers == unique_numbers:
            continue
        yield {
            from_number: to_number
            for from_number, to_number in zip(unique_numbers, substitution)
            if from_number != to_number
        }


def get_p_ab() -> SubstitutionProbabilityArray:
    """Get similarity matrix.

    The data is saved as a matrix of counts, so that s_ab gives the number of
    times that a can substitute for b in the icsd. This is be normalized,
    to give a probability of succesful substitution. See

    """
    s_ab = get_s_ab().astype(float)

    with seterr(divide="ignore", invalid="ignore"):
        tmp = s_ab ** 2 / s_ab.sum(axis=0)[None, :] / s_ab.sum(axis=1)[:, None]
    tmp[np.isnan(tmp)] = 0
    np.fill_diagonal(tmp, 1)

    return np.sqrt(tmp)


def get_s_ab() -> SubstitutionData:
    """Get substitutions."""
    name = Path(__file__).parent / "substitution.dat"
    s_ab = np.loadtxt(name).astype(int)
    return s_ab


P_AB = get_p_ab()


@log_stage("Decorate atoms")
def decorate(
    stream: Iterator[Tuple[AtomsRow, Atoms]],
    threshold: float = 0.08,
) -> Iterator[Tuple[AtomsRow, Atoms, List[SubstitutionProb]]]:
    """Create similar atomic structures.

    This recipe can substitute atoms in an atomic structure with other similar
    atoms. In this case, similarity is defined as a probability describing the
    number of experimentally known atomic structures which only differ
    by a simple substitution, say Si -> Ge.

    The number of coexisting atomic structures has been analyzed in Ref. XXX
    and this recipe is converting this number to a probability.

    The threshold option limits the number of performed atomic substitions to
    the ones that have a probability larger than the threshold.

    """
    for row, atoms in stream:
        for subs in generate_structures(atoms, P_AB, threshold=threshold):
            substituted_atoms = apply_substitution(atoms, subs)
            substitutions = []
            for i, j in subs.items():
                a = chemical_symbols[i]
                b = chemical_symbols[j]
                prob = P_AB[i, j]
                substitutions.append((a, b, prob))
            yield row, substituted_atoms, substitutions


def make_reduced_formula(atoms: Atoms) -> str:
    formula = atoms.symbols.formula
    formula = formula.reduce()[0]
    return f"{formula:abc}"


def make_atoms_spg_descriptor(atoms: Atoms, spacegroup: int):
    """Make descriptor used to identify existing materials.

    This descriptor has the format reduced_formula-spacegroup.
    """
    red_form = make_reduced_formula(atoms)
    return f"{red_form}-{spacegroup}"


def make_ehull_descriptor(atoms: Atoms):
    """Make ehull descriptor.

    Make ehull descriptor used to identify points along the hull where we don't
    want to look for new materials. The logic is that if we already know an
    existing material at this point along the hull, then we don't wnat to
    explore further.

    The descriptor looks like: reduced_formula-"ehull"

    """
    red_form = make_reduced_formula(atoms)
    return f"{red_form}-ehull"


def build_descriptors(c2db: Database) -> MutableSet[str]:
    """Build descriptors for existing materials.

    A descriptor is just a string that can be easily compared to identify if an
    identical material already exists. It is not perfect but it is a fast way to
    identify some existing materials. In this implementationm, the descriptor
    list could look like {"MoS2-194", "MoS2-ehull"} in general
    "reduced_formula_abc_sorted - spacegroup".

    Parameters
    ----------
    c2db : Database
        Database with c2db structures.
    decorated : Database
        The decorated database.

    Returns
    -------
    MutableSet[str]
        Output descriptors.
    """
    descriptors = set()

    for row in c2db.select():
        atoms = row.toatoms()
        descriptor = make_atoms_spg_descriptor(atoms, row.spacegroup)
        descriptors.add(descriptor)

    return descriptors


def build_ehull_descriptors(c2db: Database, tolerance: float = 0.01) -> MutableSet[str]:
    """Make convex hull descriptors.

    If there is a material close to the hull we also add a "ehull" descriptor to
    catch that we do not want to investigate this class of materials anymore.
    This descriptor looks like: "MoS2-ehull" and in general: reduced_formula-"ehull".

    Parameters
    ----------
    c2db : Database
        Database that has "ehull" key value pair
    tolerance : float
        Tolerance used when addressing whether a material is on the hull,
        by default 0.01.

    """
    descriptors = set()

    for row in c2db.select():
        atoms = row.toatoms()
        if "ehull" in row and row.ehull < tolerance:
            descriptor = make_ehull_descriptor(atoms)
            descriptors.add(descriptor)

    return descriptors


@log_stage("Yield database rows")
def yield_database_rows_atoms(db: Database) -> Iterator[Tuple[AtomsRow, Atoms]]:
    """Make generator that yields row, atoms tuples.

    Parameters
    ----------
    db : Database
        Input database.

    Yields
    -------
    Iterator[Tuple[AtomsRow, Atoms]]
    """
    for ir, row in enumerate(db.select()):
        timed_print(f"Treating row #{ir}")
        atoms = row.toatoms()
        atoms.calc = None
        yield row, atoms


def compute_key_value_pairs(
    stream: Iterator[Tuple[AtomsRow, Atoms, List[SubstitutionProb]]]
) -> PipelineStream:
    """Compute key value pairs relevant for later stages of pipeline

    Computes key values pairs like space group, but also notes what
    the original row.id,formula,uid,folder what for later manual
    inspection.

    Parameters
    ----------
    stream : Iterator[Tuple[AtomsRow, Atoms, List[SubstitutionProb]]]
        Input generator

    Yields
    -------
    PipelineStream
    """
    for row, proposed_atoms, substitutions in stream:
        kvp = {
            "from_id": row.id,
            "from_formula": row.formula,
            "from_uid": row.uid,
            "from_folder": row.folder,
            "nsubs": len(substitutions),
        }
        info = structureinfo.get_wrapped_function()(proposed_atoms).data
        info.pop("spglib_dataset")
        info.pop("formula")
        info["stoichiometry"] = str(info["stoichiometry"])
        kvp.update(info)
        for sub_num, (a, b, prob) in enumerate(
            sorted(substitutions, key=lambda item: item[2])
        ):
            kvp[f"sub_{sub_num}_from"] = a
            kvp[f"sub_{sub_num}_to"] = b
            kvp[f"sub_{sub_num}_prob"] = prob
        mat_fing = material_fingerprint.get_wrapped_function()(proposed_atoms)
        kvp.update(mat_fing)
        yield proposed_atoms, kvp


@log_stage("Filter duplicates with descriptor='red_form-spacegroup'")
def filter_duplicates(
    stream: PipelineStream, descriptors: MutableSet[str]
) -> PipelineStream:
    """Filter duplicates using descriptors.

    Use the descriptors set to identify missing materials.

    Parameters
    ----------
    stream : PipelineStream
        Input generator
    descriptors : MutableSet[str]
        The known descriptors

    Yields
    -------
    PipelineStream
        Filtered data
    """
    for proposed_atoms, kvp in stream:
        descriptor = make_atoms_spg_descriptor(proposed_atoms, kvp["spacegroup"])
        if descriptor in descriptors:
            continue
        descriptors.add(descriptor)
        yield proposed_atoms, kvp


def filter_stream_based_on_descriptors(
    stream: PipelineStream,
    descriptors: MutableSet[str],
    make_descriptor: Callable[[Atoms], str],
) -> PipelineStream:
    """Use descriptors to remove ehull duplicates.

    Here we want to remove proposed materials where there already exists another
    material on the convex hull with the same stoichiometry.

    Parameters
    ----------
    stream : PipelineStream
        Input generator
    descriptors : MutableSet[str]
        The known descriptors
    make_descriptor : Callable[[Atoms], str]
        A function that is called on the input atoms and generates a string descriptor.
        If this is contained `descriptors` then skip.

    Yields
    -------
    PipelineStream
        Filtered generator
    """
    for proposed_atoms, kvp in stream:
        desc = make_descriptor(proposed_atoms)
        if desc in descriptors:
            continue
        yield proposed_atoms, kvp


def write_output_database(stream: PipelineStream, outputdb: Database) -> None:
    """Write stream to output database.

    Parameters
    ----------
    stream : PipelineStream
        Data stream.
    outputdb : Database
        Output database.
    """
    for atoms, kvp in stream:
        outputdb.write(atoms=atoms, key_value_pairs=kvp)


def remove_magnetic_moments(stream: PipelineStream) -> PipelineStream:
    """Remove initial magnetic moments on structures

    Parameters
    ----------
    stream : PipelineStream
        Data stream

    Yields
    -------
    Iterator[PipelineStream]
        Output stream
    """
    for atoms, kvp in stream:
        atoms.set_initial_magnetic_moments(None)
        yield atoms, kvp


if __name__ == "__main__":
    try:
        input_threshold = float(sys.argv[1])
    except IndexError:
        input_threshold = DEFAULT_THRESHOLD  # Default value
    pipeline(threshold=input_threshold)
